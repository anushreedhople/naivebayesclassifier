This algorithm implements the Naive Bayes classifier on a sample dataset.


Naive Bayes Classifier
Let's denote the features as X and the label as y. 
As a generative model, the naive Bayes classifier makes predictions based an estimation of the joint probability P(X,y)

For each example, the predicted label  is determined by:
y = argmaxP(X,y)


In the naive Bayes classifier, we make the assumption that all features are independent given the class label. This means that the feature probabilites can be multiplied to get the joint probability.

To make our prediction, we need to keep track of  and P(y) and P(Xi,y)

Since some data combinations do not appear in our dataset, we smooth out the probability P(Xi,y) and P(y)  with Laplacian correction. Specifically, since our dataset is small, we smooth the probability with a psuedo-count of 0.1.



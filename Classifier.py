import math

class Classifier:
    def __init__(self):
        self.Ci = {}
        self.class_labels = []
        self.training_data = {}  #key is the id of the attribute and the value is an array of attribute-values
        self.test_data = []


    def add_class(self, key):
        if key not in self.Ci:
            self.Ci[key] = 1
        else:
            self.Ci[key] = self.Ci[key] + 1


    def add_test_data(self, values):
        self.test_data.append(values)


if __name__ == '__main__':

    classifier = Classifier()

    # Accept input and store locally
    file = open("input2.txt", "r")
    lines = file.readlines()
    total_lines = 0
    PCi = {}

    # Read file line by line and store test data
    for l in range(1, len(lines)):
        line = lines[l]
        data = line.rstrip().split(',')
        if int(data[len(data)-1]) != -1:
            # Add the last element of the input as class
            classifier.class_labels.append(data[len(data)-1])
            classifier.add_class(data[len(data)-1])
            for x in range(1, len(data)-1):

                if x not in classifier.training_data:
                    value = [data[x]]
                    classifier.training_data[x] = value
                else:
                    value = classifier.training_data[x]
                    value.append(data[x])
                    classifier.training_data[x] = value
            total_lines += 1
        else:
            values = []
            for x in range(1, len(data)-1):
                values.append(data[x])
            classifier.add_test_data(values)

    # Compute PCi
    for key,value in classifier.Ci.items():
        num = float(value) + 0.1
        denum = float(total_lines) + (0.1*len(classifier.Ci))
        PCi[key] = float(num)/denum

    for data in classifier.test_data:
        PXCi = {}
        for attribute_id in range(0, len(data)):
            PXiCi = {}
            # TODO:Check for laplacian error i.e. attribute value is not present in the training dataset
            for unique_class in classifier.Ci.keys():
                training_data = classifier.training_data[attribute_id+1]
                count = 0.0
                for i in range(0,len(training_data)):
                    if classifier.class_labels[i] == unique_class and training_data[i] == data[attribute_id]:
                        count += 1
                num = float(count) + 0.1
                denum = float(classifier.Ci[unique_class]) + (0.1 * len(data))
                PXiCi[unique_class] = num / denum


            for key in PXiCi:
                if key not in PXCi:
                    PXCi[key] = float(PXiCi[key])
                else:
                    PXCi[key] = float(PXCi[key]) * float(PXiCi[key])

        max = 0.0
        assigned_class = None
        for key, value in PXCi.items():
            PXCiPCi = float(value) * PCi[key]
            if float(PXCiPCi) >= float(max):
                assigned_class = key
                max  = float(PXCiPCi)
        print assigned_class




